import asyncio
import argparse
from aiohttp import web
from random import randrange


class RequestHandler:
    """
    This is a simple request handler which contains
    API functionality for web server
    """

    max_balance = 20000

    def __init__(self):
        self.balance = randrange(self.max_balance)

    async def get_balance(self):
        """
        This method just returns on a request
        client's current balance value
        """
        return web.Response(text=f"{self.balance}")

    async def set_balance(self, amount):
        """
        This is a common method that sets clinet's balance.
        It could be used in other methods
        """
        self.balance = amount
        return web.Response(
            text=f"{self.balance}"
        )

    async def add_balance(self, request):
        """
        This method get amount from the request and
        adds it to client's balance
        """
        # amount = int(request.match_info.get("amount", 0))
        # new_balance = self.balance + amount
        # print("new_balance: ", new_balance)
        # print("self.balance: ", self.balance)

        return web.Response(
            text=f"V kashu {self.balance}!!!!"
        )
        # await self.set_balance(new_balance)

    async def take_fee(self):
        """
        This method take clients fee automaticly
        each 10 seconds
        """
        while True:
            await asyncio.sleep(10)
            print("Your balance is: ", self.balance)
            print("Fee is took!")
            new_balance = self.balance - randrange(self.max_balance - 1)
            await self.set_balance(new_balance)


handler = RequestHandler()
asyncio.ensure_future(handler.take_fee())

app = web.Application()
app.add_routes([
    web.get('/get_balance', handler.get_balance),
    web.get('/add_balance/{amount}', handler.add_balance),
])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=8080, )
    args = parser.parse_args()

    web.run_app(app, port=args.port)
