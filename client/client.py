import aiohttp
import asyncio
import yaml
import time


class Client:
    """
    A simple HTTP client
    """

    min_balance = 10000
    add_balance_value = 20000

    async def get_balance(self, session, url):
        """
        This method fetches client's balance
        """
        async with session.get(url) as response:
            return await response.text()

    async def add_balance(self, session, url):
        """
        This method sets client's balance
        """
        async with session.get(url) as response:
            return await response.text()

    async def main(self, path, port):
        """
        The main method
        """
        async with aiohttp.ClientSession() as session:
            response = await self.get_balance(
                session,
                f'http://{path}:{port}/get_balance',
            )

            print(f'Your balance is: {response}')

            balance = int(response)

            # if balance < self.min_balance:
            # if True:
            #     print("balance: ", balance)

            #     new_response = await self.add_balance(
            #         session,
            #         f'http://{path}:{port}/add_balance/{self.add_balance_value}',
            #     )

            #     print(f'Balance has been got againt: {new_response}')

            #     response = await self.add_balance(
            #         session,
            #         f'http://{path}:{port}/add_balance/{self.add_balance_value}',
            #     )

            #     print("response: ", response)


if __name__ == '__main__':

    loop = asyncio.get_event_loop()

    client = Client()

    with open('client.yaml') as f:

        config = yaml.load_all(f, Loader=yaml.FullLoader)

        for item in config:

            while True:
                for server in item.get('servers', []):

                    loop.run_until_complete(
                        client.main(
                            server.get('path'),
                            server.get('port'),
                        )
                    )
                time.sleep(10)
